<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'App\Http\Controllers\AuthController@login');
    Route::post('otp', 'App\Http\Controllers\AuthController@otp');
    Route::post('signup', 'App\Http\Controllers\AuthController@signup');
    Route::post('verify', 'App\Http\Controllers\AuthController@verify');

    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'App\Http\Controllers\AuthController@logout');
        Route::get('user', 'App\Http\Controllers\AuthController@user');

        Route::post('building/add', 'App\Http\Controllers\BuildingController@add');
        Route::post('building/update', 'App\Http\Controllers\BuildingController@update');
        Route::get('building/getBuidlingInfo/{id}', 'App\Http\Controllers\BuildingController@getBuidlingInfo');
        Route::get('building/getBuidlingInfoInvoice/{id}', 'App\Http\Controllers\BuildingController@getBuidlingInfoInvoice');
        Route::get('building/getBuildingRooms/{id}', 'App\Http\Controllers\RoomController@getBuildingRooms');
        Route::get('landlord/getBuildings', 'App\Http\Controllers\BuildingController@landlordGetBuildings');
        Route::get('landlord/getLandlordSummery', 'App\Http\Controllers\AuthController@getLandlordSummery');

        Route::post('room/add', 'App\Http\Controllers\RoomController@add');
        Route::get('landlord/getRooms', 'App\Http\Controllers\RoomController@getRooms');
        Route::get('room/getRoomInfo/{id}', 'App\Http\Controllers\RoomController@getRoomInfo');
        Route::post('room/update/{id}', 'App\Http\Controllers\RoomController@update');

        Route::post('tenant/add', 'App\Http\Controllers\TenantController@add');
       Route::get('landlord/getTenants', 'App\Http\Controllers\TenantController@getTenants');
       Route::get('landlord/getTenantsByProperty/{id}', 'App\Http\Controllers\TenantController@getTenantsByProperty');

       Route::get('client/houseCategory', 'App\Http\Controllers\SettingsController@houseCategory');
       Route::post('client/addhouseCategory', 'App\Http\Controllers\SettingsController@addhouseCategory');

       

       Route::post('invoice/generateInvoice', 'App\Http\Controllers\InvoiceController@generateInvoice');
       Route::get('invoice/getPendingInvoices/{id}', 'App\Http\Controllers\InvoiceController@getPendingInvoices');
       Route::get('invoice/getPaidInvoices/{id}', 'App\Http\Controllers\InvoiceController@getPaidInvoices');
       Route::get('invoice/getDisputedInvoices/{id}', 'App\Http\Controllers\InvoiceController@getDisputedInvoices');
       Route::post('invoice/addOtherCharges', 'App\Http\Controllers\InvoiceController@addOtherCharges');
       Route::post('invoice/dispute', 'App\Http\Controllers\InvoiceController@dispute');
       Route::post('invoice/pay', 'App\Http\Controllers\InvoiceController@pay');


       Route::post('callIPRS', 'App\Http\Controllers\TenantController@callIPRS');


        //profile
        Route::post('changePassword', 'App\Http\Controllers\AuthController@changePassword');
        Route::get('user', 'App\Http\Controllers\AuthController@user');



        //Admin
        Route::get('admin_getSummery', 'App\Http\Controllers\ApiController@admin_getSummery');

        Route::get('admin_getClients', 'App\Http\Controllers\ClientController@admin_getClients');
    });
});
