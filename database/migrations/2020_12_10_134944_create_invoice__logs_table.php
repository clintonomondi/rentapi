<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoiceLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice__logs', function (Blueprint $table) {
            $table->id();
            $table->string('invoice_id');
            $table->string('amount');
            $table->string('trans_id')->nullable();
            $table->string('system_trans_id')->nullable();
            $table->string('method')->nullable();
            $table->string('paid_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice__logs');
    }
}
