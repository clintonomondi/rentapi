<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Clinton Landlord',
            'email' => 'landlord@gmail.com',
            'role' => 'landlord',
            'phone' => '0712083124',
            'status' => 'Active',
            'password' => bcrypt('landlord'),
        ]);
    }
}
