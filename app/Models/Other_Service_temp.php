<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Other_Service_temp extends Model
{
    use HasFactory;

protected $fillable=['room_id','name','amount'];

}
