<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    use HasFactory;

    protected $fillable=['invoice_no','amount','balance','room_id','issue_date','due_date','payment_month','comp_code','tenant_id','description','status','comment'];
}
