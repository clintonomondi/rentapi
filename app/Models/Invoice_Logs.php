<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invoice_Logs extends Model
{
    use HasFactory;

    protected $fillable=['invoice_id','amount','trans_id','system_trans_id','method','paid_by'];
}
