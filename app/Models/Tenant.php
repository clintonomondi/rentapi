<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tenant extends Model
{
    use HasFactory;

    protected  $fillable=['room_id','building_id','name','phone','email','id_no','people','status','comp_code'];
}
