<?php

namespace App\Http\Controllers;

use App\Models\Building;
use App\Models\HouseCategory;
use App\Models\Invoice;
use App\Models\Room;
use App\Models\Tenant;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ApiController extends Controller
{
    public  function admin_getSummery(){
        $building=Building::count();
        $rooms=Room::count();
        $tenants=Tenant::count();
        $balance=Invoice::sum('balance');
        $invoice=Invoice::orderBy('id','desc')->limit(4)->get();
        $amount=Invoice::sum('amount');
        $bal=Invoice::sum('balance');
        $total_collected=$amount-$bal;
        $year=date("Y");
        $clients=User::where('user_type','client')->where('role','admin')->count();
        $data=DB::select( DB::raw("SELECT 
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM invoices WHERE MONTH(created_at)='1' AND YEAR(created_at)='$year' )Jan,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM invoices WHERE MONTH(created_at)='2' AND YEAR(created_at)='$year' )Feb,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM invoices WHERE MONTH(created_at)='3' AND YEAR(created_at)='$year' )Mar,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM invoices WHERE MONTH(created_at)='4' AND YEAR(created_at)='$year' )Apr,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM invoices WHERE MONTH(created_at)='5' AND YEAR(created_at)='$year' )May,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM invoices WHERE MONTH(created_at)='6' AND YEAR(created_at)='$year' )Jun,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM invoices WHERE MONTH(created_at)='7' AND YEAR(created_at)='$year' )Jul,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM invoices WHERE MONTH(created_at)='8' AND YEAR(created_at)='$year' )Aug,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM invoices WHERE MONTH(created_at)='9' AND YEAR(created_at)='$year' )Sept,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM invoices WHERE MONTH(created_at)='10' AND YEAR(created_at)='$year' )Oct,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM invoices WHERE MONTH(created_at)='11' AND YEAR(created_at)='$year' )Nov,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM invoices WHERE MONTH(created_at)='12' AND YEAR(created_at)='$year' )Dece
 FROM DUAL ") );

        return ['status'=>true,'building'=>$building,'rooms'=>$rooms,'tenants'=>$tenants,'balance'=>$balance,
            'invoice'=>$invoice,'total_collected'=>$total_collected,'data'=>$data,'clients'=>$clients];

    }
}
