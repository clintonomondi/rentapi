<?php

namespace App\Http\Controllers;

use App\Models\HouseCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SettingsController extends Controller
{
    public  function houseCategory(){
        $data=HouseCategory::where('comp_code',Auth::user()->comp_code)->get();
        $count=HouseCategory::where('comp_code',Auth::user()->comp_code)->count();
        return ['status'=>true,'data'=>$data,'count'=>$count];
    }

    public  function addhouseCategory(Request $request){
      $request['comp_code']=Auth::user()->comp_code;
      $data=HouseCategory::create($request->all());
        return ['status'=>true,'message'=>'Category created succesfully'];
    }
}
