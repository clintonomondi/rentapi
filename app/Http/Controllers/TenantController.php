<?php

namespace App\Http\Controllers;

use App\Models\Room;
use App\Models\Tenant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Knox\AFT\AFT;

class TenantController extends Controller
{
    public  function add(Request $request){
        $room=Room::find($request->room_id);
        if(!empty($room->tenant_id) || $room->occupied=='Yes'){
            return ['status'=>false,'message'=>'The room is already booked'];
        }
    $request['comp_code']=Auth::user()->comp_code;
    $data=Tenant::create($request->all());
    $request['tenant_id']=$data->id;
    $request['occupied']='Yes';
    $room->update($request->all());

        if(strlen($request->phone)==10){
            $phone=$request->phone;
        }else{
            $phone=str_replace(' ','','0'.substr($request->phone,4));
        }

        $message='Your have been registered in room number '.$room->number;
        AFT::sendMessage($phone, $message,'Postman');

    return ['status'=>true,'message'=>'Tenant created successfully in room number  '.$room->number];
    }


    public  function getTenants(){
        $comp_code=Auth::user()->comp_code;
        $data = DB::select( DB::raw(" SELECT *,
 (SELECT name from buildings B WHERE B.id=A.building_id)building,
 (SELECT number from rooms B WHERE B.id=A.room_id)room,
 (SELECT cost from rooms B WHERE B.id=A.room_id)cost,
 (SELECT SUM(amount) from charges B WHERE B.room_id=A.room_id)other_cost,
 (SELECT SUM(amount) from invoices B WHERE B.tenant_id=A.id AND status='PENDING')balance
  FROM tenants A WHERE comp_code='$comp_code'  ORDER BY id DESC LIMIT 10") );
        return ['data'=>$data];
    }
    public  function getTenantsByProperty($id){
        $data = DB::select( DB::raw(" SELECT *,
 (SELECT name from buildings B WHERE B.id=A.building_id)building,
 (SELECT number from rooms B WHERE B.id=A.room_id)room,
 (SELECT cost from rooms B WHERE B.id=A.room_id)cost,
 (SELECT SUM(amount) from charges B WHERE B.room_id=A.room_id)other_cost,
 (SELECT SUM(amount) from invoices B WHERE B.tenant_id=A.id AND status='PENDING')balance
  FROM tenants A WHERE building_id='$id'  ORDER BY id DESC LIMIT 10") );
        return ['data'=>$data];
    }


    public  function callIPRS(Request $request){
        if(strlen($request->id_no)<1){
            return ['status'=>false,'message'=>'Invalid Identity number','trial'=>true];
        }
        $data=(array(
            "id_no"=>$request->id_no,
            "fname"=>'',
            "lname"=>'',
            "mname"=>'',
            "kra"=>'',
            "gender"=>'',
            "phone"=>'',
            "dob"=>'',
        ));
        return ['status'=>true,'data'=>$data,'message'=>'Query successful'];
    }


}
