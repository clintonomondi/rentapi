<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ClientController extends Controller
{
    public  function admin_getClients(){
        $clients = DB::select(DB::raw("SELECT name,email,phone,user_type,role,status,comp_code,
(SELECT COUNT(*) FROM `buildings` B WHERE B.comp_code=A.comp_code)buildings,
(SELECT COUNT(*) FROM `rooms` B WHERE B.comp_code=A.comp_code)houses
 FROM users A WHERE user_type='client' AND role='admin'"));

        return ['clients'=>$clients];
    }
}
