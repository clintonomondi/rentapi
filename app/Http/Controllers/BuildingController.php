<?php

namespace App\Http\Controllers;

use App\Models\Building;
use App\Models\HouseCategory;
use App\Models\Room;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BuildingController extends Controller
{
    public  function add(Request $request){
        $request['comp_code']=Auth::user()->comp_code;
        $data=Building::create($request->all());
        return ['status'=>true,'message'=>'Building created successfully'];
    }
    public  function update(Request $request){
       $buidling=Building::find($request->id);
       $buidling->update($request->all());
        return ['status'=>true,'message'=>'Building update successfully'];
    }

    public  function landlordGetBuildings(){
        $comp_code=Auth::user()->comp_code;
        $buildings = DB::select( DB::raw(" SELECT *,
 (SELECT count(*) FROM rooms B WHERE B.building_id=A.id)rooms
  FROM buildings A WHERE comp_code='$comp_code'") );
        return ['status'=>true,'data'=>$buildings];
    }

    public  function getBuidlingInfo($id){
        $data=Building::find($id);
        $buidlings = DB::select( DB::raw(" SELECT *,
 (SELECT name from tenants B WHERE B.room_id=A.id)name,
 (SELECT phone from tenants B WHERE B.room_id=A.id)phone,
 (SELECT SUM(amount) from charges B WHERE B.room_id=A.id)other_amount
  FROM rooms A WHERE building_id='$id' ORDER BY occupied ASC") );

        $rooms=Room::where('occupied','No')->where('building_id',$id)->get();

        $category=HouseCategory::where('comp_code',Auth::user()->comp_code)->get();
        return ['data'=>$data,'buidlings'=>$buidlings,'category'=>$category,'rooms'=>$rooms];
    }

    public function getBuidlingInfoInvoice($id){
        $data=Building::find($id);
        $buidlings = DB::select( DB::raw(" SELECT *,
 (SELECT name from tenants B WHERE B.room_id=A.id)name,
 (SELECT phone from tenants B WHERE B.room_id=A.id)phone,
 (SELECT SUM(amount) from charges B WHERE B.room_id=A.id)other_amount,
 (SELECT SUM(amount) from other__service_temps B WHERE B.room_id=A.id)other_charges
  FROM rooms A WHERE  occupied='Yes' AND building_id='$id' ORDER BY occupied ASC") );

        $category=HouseCategory::where('comp_code',Auth::user()->comp_code)->get();
        return ['data'=>$data,'buidlings'=>$buidlings,'category'=>$category];
    }
}
