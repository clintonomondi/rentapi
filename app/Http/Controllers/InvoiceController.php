<?php

namespace App\Http\Controllers;

use App\Models\Charges;
use App\Models\Invoice;
use App\Models\Invoice_Logs;
use App\Models\Other_Charges;
use App\Models\Other_Service_temp;
use App\Models\Other_services;
use App\Models\Room;
use App\Models\Tenant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Knox\AFT\AFT;

class InvoiceController extends Controller
{
    public function generateInvoice(Request $request)
    {
        if(empty($request->rooms)){
            return ['status'=>false,'message'=>'Please select any room to generate invoice'];
        }else{
            foreach ($request->rooms as $id){
                $room=Room::find($id);
                $tenant=Tenant::find($room->tenant_id);
                $other_service_amount=Other_Service_temp::where('room_id',$id)->sum('amount');
                $charges=Charges::where('room_id',$id)->sum('amount');

                $request['invoice_no'] = strtoupper(substr(md5(microtime()), 0, 10));
                $request['invoice_no'] =$room->number.mt_rand(100, 999).date("Y");
                $request['balance'] =$room->cost+$other_service_amount+$charges ;
                $request['amount'] =$room->cost+$other_service_amount+$charges ;
                $request['comp_code'] = Auth::user()->comp_code;
                $request['tenant_id'] = $room->tenant_id;
                $request['room_id'] = $id;
                $request['issue_date'] = 'NA';
                $invoice = Invoice::create($request->all());

                if($other_service_amount>0) {
                    $other_services = Other_Service_temp::where('room_id', $id)->get();
                    $request['invoice_id'] = $invoice->id;
                    foreach ($other_services as $services) {
                        $request['name'] = $services->name;
                        $request['amount'] = $services->amount;
                        $c = Other_Charges::create($request->all());
                    }
                }
                $delete=Other_Service_temp::where('room_id',$id)->delete();

                if(strlen($tenant->phone)==10){
                    $phone=$tenant->phone;
                }else{
                    $phone=str_replace(' ','','0'.substr($tenant->phone,4));
                }

                $message='An invoice Number '.$request->invoice_no.' Amount Ksh '.$request->balance.' Has been generated for your room '.$room->number;
                AFT::sendMessage($phone, $message,'Postman');
            }
        }

        return ['status' => true, 'message' => 'Invoice generated successfully'];
    }

    public function getPendingInvoices($id)
    {
        $invoices = DB::select(DB::raw("SELECT * ,
(SELECT number from rooms B WHERE B.id=A.room_id )room,
(SELECT phone from tenants B WHERE B.id=A.tenant_id )phone,
(SELECT name from tenants B WHERE B.id=A.tenant_id )tenant_name,
 (SELECT name FROM buildings B WHERE B.id=(SELECT building_id FROM tenants WHERE id=A.tenant_id))building
FROM `invoices` A WHERE status='PENDING' AND tenant_id IN (SELECT id FROM tenants WHERE building_id='$id')"));
        return ['status' => true, 'invoices' => $invoices];
    }
    public function getPaidInvoices($id)
    {
        $invoices = DB::select(DB::raw("SELECT * ,
(SELECT number from rooms B WHERE B.id=A.room_id )room,
(SELECT phone from tenants B WHERE B.id=A.tenant_id )phone,
(SELECT name from tenants B WHERE B.id=A.tenant_id )tenant_name,
 (SELECT name FROM buildings B WHERE B.id=(SELECT building_id FROM tenants WHERE id=A.tenant_id))building
FROM invoices A WHERE  status='PAID' AND tenant_id IN (SELECT id FROM tenants WHERE building_id='$id')"));
        return ['status' => true, 'invoices' => $invoices];
    }
    public function getDisputedInvoices($id)
    {
        $invoices = DB::select(DB::raw("SELECT * ,
(SELECT number from rooms B WHERE B.id=A.room_id )room,
(SELECT phone from tenants B WHERE B.id=A.tenant_id )phone,
(SELECT name from tenants B WHERE B.id=A.tenant_id )tenant_name,
 (SELECT name FROM buildings B WHERE B.id=(SELECT building_id FROM tenants WHERE id=A.tenant_id))building
FROM invoices A WHERE  status='DISPUTED' AND tenant_id IN (SELECT id FROM tenants WHERE building_id='$id')"));
        return ['status' => true, 'invoices' => $invoices];
    }

    public function addOtherCharges(Request $request){
       $data=Other_Service_temp::create($request->all());
       return ['status'=>true,'message'=>'Data added successfully'];
    }


    public  function dispute(Request $request){
      $invoice=Invoice::find($request->id);
      $request['status']='DISPUTED';
      $invoice->update($request->all());

       $tenant = Tenant::find($invoice->tenant_id);

        if(strlen($tenant->phone)==10){
            $phone=$tenant->phone;
        }else{
            $phone=str_replace(' ','','0'.substr($tenant->phone,4));
        }

        $message='Hello, Invoice No' . ' ' . $invoice->invoice_no . ' has been disputed  Amount Ksh' . $invoice->balance;
        AFT::sendMessage($phone, $message,'Postman');

        return ['status'=>true,'message'=>'Invoice updated successfully'];
    }

    public  function pay(Request $request){
        $invoice=Invoice::find($request->id);
        $balance=$invoice->balance-$request->amount_to_pay;
        if($balance>0){
            $request['status']='PENDING';
        }else{
            $request['status']='PAID';
        }
        $request['balance']=$balance;
        $invoice->update($request->all());
        $request['system_trans_id']=substr(md5(microtime()), 0, 10);
        $request['amount']=$request->amount_to_pay;
        $request['invoice_id']=$invoice->id;
        $data=Invoice_Logs::create($request->all());

        $tenant = Tenant::find($invoice->tenant_id);
        if(strlen($tenant->phone)==10){
            $phone=$tenant->phone;
        }else{
            $phone=str_replace(' ','','0'.substr($tenant->phone,4));
        }

        $message='Hello, Invoice No' . ' ' . $invoice->invoice_no . ' has been paid  Amount Ksh' . $request->amount_to_pay.' balance is Ksh'.$balance;
        AFT::sendMessage($phone, $message,'Postman');

        return ['status'=>true,'message'=>'Invoice paid successfully'];

    }
}
