<?php

namespace App\Http\Controllers;

use App\Models\Building;
use App\Models\Charges;
use App\Models\HouseCategory;
use App\Models\Room;
use App\Models\Tenant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class RoomController extends Controller
{
    public  function add(Request $request){
        $validatedData = $request->validate([
            'cost' => 'required|numeric',
            'number' => 'required',
        ]);
        $request['comp_code']=Auth::user()->comp_code;
        $data=Room::create($request->all());
        if(!empty($request->charges)){
                   foreach ($request->charges as $charges){
                       $request['name']=$charges['name'];
                       $request['amount']=$charges['amount'];
                       $request['room_id']=$data->id;
               $c=Charges::create($request->all());
            }
        }
        return ['status'=>true,'message'=>'Room added  successfully'];

    }

    public  function update(Request $request,$id){
        $room=Room::find($id);
        $room->update($request->all());
        return ['status'=>true,'message'=>'Room updated  successfully'];
    }

    public  function getRooms(){
        $comp_code=Auth::user()->comp_code;
        $data = DB::select( DB::raw(" SELECT *,
 (SELECT name from buildings B WHERE B.id=A.building_id)building,
 (SELECT name from tenants B WHERE B.room_id=A.id)name,
 (SELECT phone from tenants B WHERE B.room_id=A.id)phone,
 (SELECT SUM(amount) from charges B WHERE B.room_id=A.id)other_cost
  FROM rooms A WHERE comp_code='$comp_code'  ORDER BY occupied ASC LIMIT 10") );
        return ['data'=>$data];
    }

    public  function getBuildingRooms($id){
        $data = DB::select( DB::raw(" SELECT *,
 (SELECT name from buildings B WHERE B.id=A.building_id)building,
 (SELECT name from tenants B WHERE B.room_id=A.id)name,
 (SELECT phone from tenants B WHERE B.room_id=A.id)phone,
 (SELECT SUM(amount) from charges B WHERE B.room_id=A.id)other_cost
  FROM rooms A WHERE building_id='$id'  ORDER BY occupied ASC") );
        return ['rooms'=>$data];
    }

    public  function getRoomInfo($id){
        $data=Room::find($id);
        $room = DB::select( DB::raw("SELECT *,
(SELECT NAME FROM buildings B WHERE B.id=A.building_id)building
 FROM rooms A WHERE id='$id'") );
        $charges=Charges::where('room_id',$id)->get();
        $charges_cost=Charges::where('room_id',$id)->sum('amount');
        $user=Tenant::where('room_id',$id)->first();
        $category=HouseCategory::where('comp_code',Auth::user()->comp_code)->get();
        return ['room'=>$room,'charges'=>$charges,'charges_cost'=>$charges_cost,'tenant'=>$user,'category'=>$category];
    }
}
