<?php

namespace App\Http\Controllers;

use App\Models\Building;
use App\Models\HouseCategory;
use App\Models\Invoice;
use App\Models\OTP;
use App\Models\Room;
use App\Models\Tenant;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Knox\AFT\AFT;

class AuthController extends Controller
{
    public function signup(Request $request)
    {
        $credentials = request(['email', 'password']);

       $email=User::where('email',$request->email)->count();
       if($email>0){
           return ['status'=>false,'message'=>'Email is already in use'];
       }
        $request['password']=bcrypt($request->password);
        $request['comp_code']=mt_rand(100000, 999999).'_'.date("m").'_'.date("Y");;
        $user=User::create($request->all());

        if(!Auth::attempt($credentials)) {
            return ['status' => false, 'message' => 'We are not able to log you in, please use login page'];
        }
        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();
        $profile=User::find(Auth::user()->id);
        return ['status'=>true,'user'=>$profile,'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()];


    }


    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);
        $credentials = request(['email', 'password']);
        if(!Auth::attempt($credentials))
        return ['status'=>false,'message' => 'Invalid email or password'];

        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me)
        $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();
        $profile=User::find(Auth::user()->id);
        return ['status'=>true,'user'=>$profile,'access_token' => $tokenResult->accessToken,
        'token_type' => 'Bearer',
        'expires_at' => Carbon::parse(
            $tokenResult->token->expires_at
        )->toDateTimeString()

    ];
    }


    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
        'message' => 'Successfully logged out'
    ]);
    }



    public  function getLandlordSummery(){
        $building=Building::where('comp_code',Auth::user()->comp_code)->count();
        $rooms=Room::where('comp_code',Auth::user()->comp_code)->count();
        $tenants=Tenant::where('comp_code',Auth::user()->comp_code)->count();
        $balance=Invoice::where('comp_code',Auth::user()->comp_code)->sum('balance');
        $invoice=Invoice::orderBy('id','desc')->where('comp_code',Auth::user()->comp_code)->where('status','PENDING')->limit(4)->get();
        $amount=Invoice::where('comp_code',Auth::user()->comp_code)->sum('amount');
        $bal=Invoice::where('comp_code',Auth::user()->comp_code)->sum('balance');
        $total_collected=$amount-$bal;
        $count=HouseCategory::where('comp_code',Auth::user()->comp_code)->count();
        $comp_code=Auth::user()->comp_code;
        $year=date("Y");
        $data=DB::select( DB::raw("SELECT
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM invoices WHERE MONTH(created_at)='1' AND YEAR(created_at)='$year' AND comp_code='$comp_code')Jan,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM invoices WHERE MONTH(created_at)='2' AND YEAR(created_at)='$year' AND comp_code='$comp_code')Feb,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM invoices WHERE MONTH(created_at)='3' AND YEAR(created_at)='$year' AND comp_code='$comp_code')Mar,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM invoices WHERE MONTH(created_at)='4' AND YEAR(created_at)='$year' AND comp_code='$comp_code')Apr,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM invoices WHERE MONTH(created_at)='5' AND YEAR(created_at)='$year' AND comp_code='$comp_code')May,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM invoices WHERE MONTH(created_at)='6' AND YEAR(created_at)='$year' AND comp_code='$comp_code')Jun,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM invoices WHERE MONTH(created_at)='7' AND YEAR(created_at)='$year' AND comp_code='$comp_code')Jul,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM invoices WHERE MONTH(created_at)='8' AND YEAR(created_at)='$year' AND comp_code='$comp_code')Aug,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM invoices WHERE MONTH(created_at)='9' AND YEAR(created_at)='$year' AND comp_code='$comp_code')Sept,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM invoices WHERE MONTH(created_at)='10' AND YEAR(created_at)='$year' AND comp_code='$comp_code')Oct,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM invoices WHERE MONTH(created_at)='11' AND YEAR(created_at)='$year' AND comp_code='$comp_code')Nov,
(SELECT IF(COUNT(*)=0,'1',COUNT(*)) FROM invoices WHERE MONTH(created_at)='12' AND YEAR(created_at)='$year' AND comp_code='$comp_code')Dece
 FROM DUAL ") );

        return ['status'=>true,'building'=>$building,'rooms'=>$rooms,'tenants'=>$tenants,'balance'=>$balance,
            'invoice'=>$invoice,'total_collected'=>$total_collected,'count'=>$count,'data'=>$data];
    }

    public  function otp(Request $request){
        if(strlen($request->phone)<=9){
            return ['status'=>false,'message'=>'Invalid phone number'];
        }
        if(strlen($request->phone)==10){
            $phone=$request->phone;
        }else{
            $phone=str_replace(' ','','0'.substr($request->phone,4));
        }
        $check=User::where('phone',$phone)->orWhere('phone',$request->phone)->count();
        if($check>0){
            return ['status'=>false,'message'=>'The phone number is already in use'];
        }
        $request['phone']=$phone;
        $randomid = mt_rand(100000,999999);
        $request['code']=$randomid;
        $data=OTP::create($request->all());

        $message='Your renetal six digit  verification code is '.$randomid;
        AFT::sendMessage($phone, $message,'Postman');

        return ['status'=>true,'message'=>'A six digit verification code has been sent,enter the code','phone'=>$phone];

    }

    public function verify(Request $request){
        $datas = DB::select( DB::raw("SELECT * FROM `o_t_p_s` WHERE phone='$request->phone' AND code='$request->code' AND created_at > NOW() - INTERVAL 4 HOUR") );
        if($datas==null){
            return ['status'=>false,'message'=>'Invalid code'];
        }
        return ['status'=>true,'message'=>'Success'];
    }

    public  function user(){
        $user=User::find(Auth::user()->id);

        return ['user'=>$user];
    }

    public  function changePassword(Request $request){
        if($request->password!=$request->repass){
            return ['status'=>false,'message'=>'Password do not match'];
        }

        $currentpass = auth()->user()->password;
        if (!Hash::check($request['currentpass'], $currentpass)) {
            return ['status'=>false,'message'=>'The current password is invalid'];
        }
        $request['password']=bcrypt($request->password);
        $request['password_changed']='Yes';
        $user=User::find(Auth::user()->id);
        $user->update($request->all());

        if(strlen($user->phone)==10){
            $phone=$user->phone;
        }else{
            $phone=str_replace(' ','','0'.substr($user->phone,4));
        }

        $message='Your password has been successfully changed.If this was not you please contact  Support center ';
        AFT::sendMessage($phone, $message,'Postman');
        return ['status'=>true,'message'=>'Password successfully set, you will now use your new password to login'];
    }
}
